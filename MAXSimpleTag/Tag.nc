includes maxMsg;

configuration Tag
{
	//No interface provided
}
implementation
{
	components Main, TagM, TimerC, LedsC, GenericComm, StringStore;

	Main.StdControl -> TagM;
	Main.StdControl -> TimerC;

	TagM.Leds -> LedsC;
	TagM.Timer -> TimerC.Timer[unique("Timer")];

	//String Store
	TagM.StringStoreControl -> StringStore;
	TagM.StringCtrl -> StringStore;

	//Communication Control
	TagM.CommControl -> GenericComm;
	TagM.ReceiveProgram -> GenericComm.ReceiveMsg[AM_MAXPROGRAM];
	TagM.SendProgramAck -> GenericComm.SendMsg[AM_MAXPROGRAMACK];
	TagM.ReceiveQuery -> GenericComm.ReceiveMsg[AM_MAXQUERY];
	TagM.SendAns -> GenericComm.SendMsg[AM_MAXANSWER];
	TagM.ReceiveDesc -> GenericComm.ReceiveMsg[AM_MAXDESCQUERY];
	TagM.SendDesc -> GenericComm.SendMsg[AM_MAXDESCANSWER];
	TagM.SendDescEnd -> GenericComm.SendMsg[AM_MAXDESCEND];
	TagM.ReceiveReset -> GenericComm.ReceiveMsg[AM_MAXRESET];
}
