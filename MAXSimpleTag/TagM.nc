includes maxMsg;
includes EEPROM;

module TagM
{
	provides interface StdControl;
	uses
	{
		interface Timer;
		interface Leds;

		interface StdControl as StringStoreControl;
		interface StringCtrl;

		interface StdControl as CommControl;
		interface ReceiveMsg as ReceiveProgram;
		interface SendMsg as SendProgramAck;
		interface ReceiveMsg as ReceiveQuery;
		interface SendMsg as SendAns;
		interface ReceiveMsg as ReceiveDesc;
		interface SendMsg as SendDesc;
		interface SendMsg as SendDescEnd;
		interface ReceiveMsg as ReceiveReset;
	}
}
implementation
{
	//Declarations
	//************
	TOS_Msg descMsg;
	char descWord[WORD_SIZE];
	bool descLock;
	int descCount;

	TOS_Msg ansMsg;
	bool queryLock;
	char *queryWord;
	char readResult[WORD_SIZE];
	uint8_t currWord;
	bool queryResult;

	TOS_Msg ackMsg;
	char *programWord;
	bool programLock;

	//Tasks
	//*****
	//Reset System
	task void reset()
	{
		call StringCtrl.reset();
	}

	//Write Word to Buffer
	task void writeWord()
	{
		call StringCtrl.write(programWord);
	}

	//Send Acknowledgement to Program
	task void sendProgramAck()
	{
		struct maxProgramAck *ack;

		//Allocate payload to message
		atomic
		{
			ack = (struct maxProgramAck *)ackMsg.data;
		}

		//Attempt to send message
		if (call SendProgramAck.send(TOS_BCAST_ADDR, sizeof(struct maxProgramAck), &ackMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Descriptor End
	task void sendDescEnd()
	{
		struct maxDescEnd *descEnd;

		//Allocate payload
		atomic
		{
			descEnd = (struct maxDescEnd *)descMsg.data;
		}

		//Set payload value
		descEnd->sourceMoteID = TOS_LOCAL_ADDRESS;

		//Attempt to send message
		if (call SendDescEnd.send(TOS_BCAST_ADDR, sizeof(struct maxDescEnd), &descMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Next Descriptor Word
	task void sendNextDesc()
	{
		if (descCount == 0)
		{
			descLock = FALSE;
			post sendDescEnd();
		}
		else
		{
			call StringCtrl.read(descCount-1, descWord);
		}
	}

	//Send Descriptor
	task void sendDesc()
	{
		struct maxDescAnswer *descAns;

		//Allocate payload
		atomic
		{
			descAns = (struct maxDescAnswer *)descMsg.data;
		}

		//Set payload value
		descAns->sourceMoteID = TOS_LOCAL_ADDRESS;
		strcpy(descAns->data, descWord);

		//Attempt to send message
		if (call SendDesc.send(TOS_BCAST_ADDR, sizeof(struct maxDescAnswer), &descMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Answer to Query
	task void sendAns()
	{
		struct maxAnswer *ans;

		//Allocate payload to message
		atomic	
		{
			ans = (struct maxAnswer *)ansMsg.data;
		}

		//Set the payload	
		ans->sourceMoteID = TOS_LOCAL_ADDRESS;
		ans->result = queryResult;

		//Attempt to send message
		if (call SendAns.send(TOS_BCAST_ADDR, sizeof(struct maxAnswer), &ansMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Check Next Word
	task void checkWord()
	{
		if (currWord == 0)
		{
			queryLock = FALSE;
			post sendAns();
		}
		else
		{
			call StringCtrl.read(currWord-1, readResult);
		}
	}

	//Query Data
	task void query()
	{
		currWord = call StringCtrl.size();
		queryResult = FALSE;

		post checkWord();
	}

	//StringStore Events
	//******************
	//Write Done
	event result_t StringCtrl.writeDone(result_t success)
	{
		programLock = FALSE;
		post sendProgramAck();

		return SUCCESS;
	}

	//Read Done
	event result_t StringCtrl.readDone(char *buffer, result_t success)
	{
		if (success == SUCCESS)
		{
			//Query Read
			if (queryLock)
			{
				if (strcmp(buffer,queryWord) == 0)
					queryResult = TRUE;

				currWord--;
				post checkWord();
			}

			//Descriptor Read
			if (descLock)
			{
				post sendDesc();
			}
		}

		return SUCCESS;
	}

	//GenericComm Events
	//******************
	//Receive Descriptor Query
	event TOS_MsgPtr ReceiveDesc.receive(TOS_MsgPtr msgPtr)
	{
		if (!descLock)
		{
			atomic
			{
				descLock = TRUE;
				descCount = call StringCtrl.size();
			}
			post sendNextDesc();
		}

		return msgPtr;
	}

	//Receive Reset
	event TOS_MsgPtr ReceiveReset.receive(TOS_MsgPtr msgPtr)
	{
		post reset();

		return msgPtr;
	}

	//Receive MaxProgram then Send Reply
	event TOS_MsgPtr ReceiveProgram.receive(TOS_MsgPtr msgPtr)
	{
		struct maxProgram *program= (struct maxProgram *)msgPtr->data; 

		if (!programLock)
		{
			atomic
			{
				programLock = TRUE;
				programWord = program->data;
			}
			post writeWord();
		}

		return msgPtr;
	}

	//Acknowledge send descriptor end
	event result_t SendDescEnd.sendDone(TOS_MsgPtr sent, result_t success)
	{
		return SUCCESS;
	}

	//Acknowledge send descriptor done
	event result_t SendDesc.sendDone(TOS_MsgPtr sent, result_t success)
	{
		descCount--;
		post sendNextDesc();
		
		return SUCCESS;
	}

	//Acknowledge send program acknowledgement done
	event result_t SendProgramAck.sendDone(TOS_MsgPtr sent, result_t success)
	{
		return SUCCESS;
	}

	//Acknowledge send answer done
	event result_t SendAns.sendDone(TOS_MsgPtr sent, result_t success)
	{
		return SUCCESS;
	}

	//Receive MaxQuery then Send Reply
	event TOS_MsgPtr ReceiveQuery.receive(TOS_MsgPtr msgPtr)
	{
		struct maxQuery *queryMsgData= (struct maxQuery *)msgPtr->data; 

		if (!queryLock)
		{
			atomic
			{
				queryLock = TRUE;
				queryWord = queryMsgData->data;
			}
			post query();
		}

		return msgPtr;
	}

	//Timer Events
	//************
	//Timer fired
	event result_t Timer.fired()
	{
		//Toggle LED
		call Leds.yellowToggle();

		return SUCCESS;
	}

	//StdControl Interface
	//********************
	//Initialise Component
	command result_t StdControl.init()
	{
		//Initialise Leds
		call Leds.init();
		call Leds.yellowOff();
		call Leds.greenOff();
		call Leds.redOff();

		//Initialise GenericComm
		call CommControl.init();

		//Initialise EEPROM
		programLock = FALSE;
		queryLock = FALSE;
		descLock = FALSE;
		call StringStoreControl.init();

		return SUCCESS;
	}

	//Start Component
	command result_t StdControl.start()
	{
		//Start Timer
		call Timer.start(TIMER_REPEAT, 1000);

		//Start GenericComm
		call CommControl.start();

		//Start EEPROM
		call StringStoreControl.start();

		return SUCCESS;
	}

	//Stop Component
	command result_t StdControl.stop()
	{
		//Stop Timer
		call Timer.stop();

		//Stop GenericComm
		call CommControl.stop();

		//Stop EEPROM
		call StringStoreControl.stop();

		return SUCCESS;
	}
}
