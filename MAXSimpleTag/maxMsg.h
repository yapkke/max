enum
{
  WORD_SIZE = 20,
  REPORT_SIZE = 10
};

struct maxQuery
{
  uint16_t sourceMoteID;
  char data[WORD_SIZE];
};

struct maxAnswer
{
  uint16_t sourceMoteID;
  bool result;
};

struct maxDescQuery
{
  uint16_t sourceMoteID;
};

struct maxDescAnswer
{
  uint16_t sourceMoteID;
  char data[WORD_SIZE];
};

struct maxDescEnd
{
  uint16_t sourceMoteID;
};

struct maxReport
{
  uint16_t sourceMoteID;
  uint16_t reportedMoteID;
  uint16_t RSSI;
};

struct maxProgram
{
  uint16_t programMoteID;
  char data[WORD_SIZE];
};

struct maxProgramAck
{
  bool result;
};

struct maxSDescEnd
{
};

struct maxReset
{
};

enum
{
  AM_MAXQUERY = 10,
  AM_MAXANSWER = 11,
  AM_MAXDESCQUERY = 12,
  AM_MAXDESCANSWER = 13,
  AM_MAXDESCEND = 14,
  AM_MAXREPORT = 15,
  AM_MAXPROGRAM = 16,
  AM_MAXPROGRAMACK = 17,
  AM_MAXSDESCEND = 18,
  AM_MAXRESET = 32
};
