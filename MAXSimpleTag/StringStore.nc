includes EEPROM;

configuration StringStore
{
	provides
	{
		interface StdControl;
		interface StringCtrl;
	}
}
implementation
{
	components StringStoreM, EEPROM;

	StdControl = StringStoreM;
	StringCtrl = StringStoreM;

	StringStoreM.EEPROMControl -> EEPROM;
	StringStoreM.EEPROMWrite -> EEPROM.EEPROMWrite[unique("EEPROMWrite")];
	StringStoreM.EEPROMRead -> EEPROM;
}
