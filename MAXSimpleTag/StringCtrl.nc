//Define String Control Interface for StringStore
interface StringCtrl
{
	//Commands
	//********
	//Command to return current size
	command uint8_t size();

	//Command to reset buffer
	command result_t reset();

	//Command to write string
	command result_t write(char *buffer);

	//Commmand to read string
	command result_t read(uint8_t pos, char *buffer);

	//Events
	//******
	//Write Done
	event result_t writeDone(result_t success);

	//Read Done
	event result_t readDone(char *buffer, result_t success);
}
