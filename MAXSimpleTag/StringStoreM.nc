includes EEPROM;
includes maxMsg;

module StringStoreM
{
	provides
	{
		interface StdControl;
		interface StringCtrl;
	}
	uses
	{
		interface StdControl as EEPROMControl;
		interface EEPROMWrite;
		interface EEPROMRead;
	}
}
implementation
{
	//Variables
	//*********
	char words[10][WORD_SIZE];
	uint8_t currWord;

	//String Control
	//**************
	//Return current size
	command uint8_t StringCtrl.size()
	{
		return currWord;
	}

	//Reset
	command result_t StringCtrl.reset()
	{
		currWord = 0;

		return SUCCESS;
	}

	//Write Word
	command result_t StringCtrl.write(char *buffer)
	{
		strcpy(words[currWord],buffer);
		currWord++;

		return signal StringCtrl.writeDone(SUCCESS);
	}

	//Read Word
	command result_t StringCtrl.read(uint8_t pos, char *buffer)
	{
		if (pos < currWord)
		{
			strcpy(buffer, words[pos]);
			return signal StringCtrl.readDone(buffer, SUCCESS);
		}
		else
			return signal StringCtrl.readDone(buffer, FAIL);
	}

	//EEPROM Read Events
	//******************
	//EEPROM Read Done
	event result_t EEPROMRead.readDone(uint8_t *buffer, result_t success)
	{
		return SUCCESS;
	}

	//EEPROM Write Events
	//*******************
	//EEPROM Write Done
	event result_t EEPROMWrite.writeDone(uint8_t *buffer)
	{
		return SUCCESS;
	}

	//EEPROM Write Ends
	event result_t EEPROMWrite.endWriteDone(result_t success)
	{
		return SUCCESS;
	}

	//Standard Control Interface
	//**************************
	//Initialisation
	command result_t StdControl.init()
	{
		return call EEPROMControl.init();
	}

	//Start
	command result_t StdControl.start()
	{
		currWord = 0;
		return call EEPROMControl.start();
	}

	//Stop
	command result_t StdControl.stop()
	{
		return call EEPROMControl.stop();
	}
}
