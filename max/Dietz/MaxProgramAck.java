/**
 * This class is automatically generated by mig. DO NOT EDIT THIS FILE.
 * This class implements a Java interface to the 'MaxProgramAck'
 * message type.
 */

package max.Dietz;

public class MaxProgramAck extends net.tinyos.message.Message {

    /** The default size of this message type in bytes. */
    public static final int DEFAULT_MESSAGE_SIZE = 1;

    /** The Active Message type associated with this message. */
    public static final int AM_TYPE = 17;

    /** Create a new MaxProgramAck of size 1. */
    public MaxProgramAck() {
        super(DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /** Create a new MaxProgramAck of the given data_length. */
    public MaxProgramAck(int data_length) {
        super(data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck with the given data_length
     * and base offset.
     */
    public MaxProgramAck(int data_length, int base_offset) {
        super(data_length, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck using the given byte array
     * as backing store.
     */
    public MaxProgramAck(byte[] data) {
        super(data);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck using the given byte array
     * as backing store, with the given base offset.
     */
    public MaxProgramAck(byte[] data, int base_offset) {
        super(data, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck using the given byte array
     * as backing store, with the given base offset and data length.
     */
    public MaxProgramAck(byte[] data, int base_offset, int data_length) {
        super(data, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck embedded in the given message
     * at the given base offset.
     */
    public MaxProgramAck(net.tinyos.message.Message msg, int base_offset) {
        super(msg, base_offset, DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new MaxProgramAck embedded in the given message
     * at the given base offset and length.
     */
    public MaxProgramAck(net.tinyos.message.Message msg, int base_offset, int data_length) {
        super(msg, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
    /* Return a String representation of this message. Includes the
     * message type name and the non-indexed field values.
     */
    public String toString() {
      String s = "Message <MaxProgramAck> \n";
      try {
        s += "  [result=0x"+Long.toHexString(get_result())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      return s;
    }

    // Message-type-specific access methods appear below.

    /////////////////////////////////////////////////////////
    // Accessor methods for field: result
    //   Field type: short, unsigned
    //   Offset (bits): 0
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'result' is signed (false).
     */
    public static boolean isSigned_result() {
        return false;
    }

    /**
     * Return whether the field 'result' is an array (false).
     */
    public static boolean isArray_result() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'result'
     */
    public static int offset_result() {
        return (0 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'result'
     */
    public static int offsetBits_result() {
        return 0;
    }

    /**
     * Return the value (as a short) of the field 'result'
     */
    public short get_result() {
        return (short)getUIntElement(offsetBits_result(), 8);
    }

    /**
     * Set the value of the field 'result'
     */
    public void set_result(short value) {
        setUIntElement(offsetBits_result(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'result'
     */
    public static int size_result() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'result'
     */
    public static int sizeBits_result() {
        return 8;
    }

}
