package max.Dietz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import net.tinyos.message.*;

/**
   Listener for MAX Dietz which implements event-driven actions
 */
public class MAXListener
    implements ActionListener, MessageListener
{
    //Members
    //*******
    QueryBar queryBar;
    MAXActor maxAct;

    //Methods
    //*******
    /** Initialise MAX Listener
     */
    public void init(QueryBar queryBar, MAXActor maxAct)
    {
	//Link GUI Components
	this.queryBar = queryBar;

	//Link MAX Actor
	this.maxAct = maxAct;
    }

    /** MoteIF's Messenger Listener
     */
    public void messageReceived(int destAddr, Message msg)
    {
	if (msg instanceof MaxAnswer)
	    maxAct.receiveAnswer(destAddr, (MaxAnswer)msg);
	else if (msg instanceof MaxDescAnswer)
	    maxAct.receiveDescAnswer(destAddr, (MaxDescAnswer)msg);
	else if (msg instanceof MaxDescEnd)
	    maxAct.receiveDescEnd(destAddr, (MaxDescEnd)msg);
	else if (msg instanceof MaxReport)
	    maxAct.receiveReport(destAddr, (MaxReport)msg);
	else if (msg instanceof MaxSDescEnd)
	    maxAct.receiveSDescEnd(destAddr, (MaxSDescEnd)msg);
	else
	    throw new RuntimeException("Message of unknown class received: "+msg);
    }

    /** Action Listener Function
     */
    public void actionPerformed(ActionEvent actionEvent)
    {
	//Search Button Clicked
	if (actionEvent.getActionCommand() == queryBar.buttonString)
	{
	    maxAct.sendQuery(queryBar.text.getText());
	}
    }
}
