package max.Dietz;

import java.io.*;
import java.util.*;
import net.tinyos.util.*;
import net.tinyos.message.*;

/**
   Class to listen for program's acknowledgements.
 */
public class ProgramListen
    implements MessageListener
{
    //Members
    //*******
    /** Mote Interface for programming
     */
    public MoteIF mote;
    /** String tokenizer to parse descriptor into words
     */
    public StringTokenizer strings;
    /** ID of mote to be programmed
     */
    public int moteID;

    //Methods
    //*******
    /** MoteIF's Messenger Listener
     */
    public void messageReceived(int destAddr, Message msg)
    {
	if (msg instanceof MaxProgramAck)
	{
	    System.out.println("Acknowledged");
	    this.sendNext();
	}
	else
	    throw new RuntimeException("Message of unknown class received: "+msg);
    }

    /** Send next token if any left
     */
    public void sendNext()
    {
	MaxProgram programMsg = new MaxProgram();

	if (strings.hasMoreTokens())
	{
	    //Send Next Token
	    programMsg.setString_data(strings.nextToken().toLowerCase());
	    try
	    {
		mote.send(moteID, programMsg);
	    }
	    catch (IOException ioException)
	    {
		System.err.println(ioException);
	    }
	}
	else
	{
	    //Exit upon completion
	    System.exit(0);
	}
    }
}
