package max.Dietz;

import java.io.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;

/**
   The main class for MAX Dietz Prototype.
   @author ykk
 */
public class MAX 
    extends JApplet
{
    //Members
    //*******
    static MAXFrame mainFrame = new MAXFrame("MAX - Dietz");
    static MAXListener maxListen = new MAXListener();
    static MAXActor maxAct = new MAXActor();

    //Methods
    //*******
    /** MAX Dietz's main running function.
     */
    public static void main(String[] args)
    {
	//Initialise Main Frame
	mainFrame.init(maxListen);

	//Initialise Listener-Actor
	maxListen.init(mainFrame.queryBar, maxAct);
	maxAct.init(maxListen, mainFrame);
    }
}
