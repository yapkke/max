package max.Dietz;

import java.awt.event.*;

/**
   Class to Handle Closing of Main Window
 */
class WinCloseEventHandler
    extends WindowAdapter
{
    /** Handles closing of windows
     */
    public void windowClosing(WindowEvent winEvt)
    {
	System.exit(0);
    }
}
