package max.Dietz;

import java.util.*;

public class MAXResultVector
    extends Vector
{
    //Members
    //*******
    Vector stationVector;

    //Methods
    //*******
    /** Add report of signal strength
     */
    public void addReport(int moteID, int reportMote, int strength)
    {
	MAXResult result;
	int moteNo = moteIndex(moteID);

	if (moteNo != -1)
	{
	    //Add Strength Report
	    result = (MAXResult) super.get(moteNo);
	    result.addReport(reportMote, strength, stationVector);

	    //Add to Station Vector
	    if (stationIndex(reportMote) == -1)
		stationVector.add(new MAXStation(reportMote));
	}
    }

    /** Check if Station Exist
     */
    public int stationIndex(int stationNo)
    {
	MAXStation tmpStation;
	int i;

	for (i = 0; i < stationVector.size(); i++)
	{
	    tmpStation = (MAXStation) stationVector.get(i);
	    if (tmpStation.statID == stationNo)
		return i;
	}

	return -1;
    }

    /** Link station vector
     */
    public void linkStationVector(Vector stationVector)
    {
	this.stationVector = stationVector;
    }

    /** Add Station Descriptor
     */
    public void addStationDesc(int stationNo, String descWord)
    {
	MAXStation station;
	int statNo = stationIndex(stationNo);

	if (statNo != -1)
	{
    	    //Append Descriptor Word
	    station = (MAXStation) stationVector.get(statNo);
	    station.addDesc(descWord);
	}
    }

    /** Add Descriptor
     */
    public void addDesc(int moteID, String descWord)
    {
	MAXResult result;
	int moteNo = moteIndex(moteID);

	if (moteNo != -1)
	{
    	    //Append Descriptor Word
	    result = (MAXResult) super.get(moteNo);
	    result.addDesc(descWord);
	}
    }

    /** Add Result
     */
    public void add(int moteID, short moteResult)
    {
	MAXResult result;
	int moteNo = moteIndex(moteID);
	
	if (moteNo == -1)
	{
	    //Add New Result
	    result = new MAXResult();
	    result.moteID = moteID;
	    result.resultCount = moteResult;
	    super.add(result);
	}
	else
	{
	    //Append Result
	    result = (MAXResult) super.get(moteNo);
	    result.resultCount += moteResult;
	}
    }

    /** Check if Mote ID exist
     */
    public int moteIndex(int moteID)
    {
	MAXResult result;

	for (int i = 0; i < super.size(); i++)
	{
	    result = (MAXResult) super.get(i);
	    if (result.moteID == moteID)
		return i;
	}

	return -1;
    }
}
