package max.Dietz;

import java.io.*;
import java.util.*;
import net.tinyos.util.*;
import net.tinyos.message.*;

/**
   Actor for MAX Dietz which implements operations
 */
public class MAXActor
{
    //Members
    //*******
    /** Group ID
     */
    public final int groupID = 0x13;
    MoteIF mote;
    MAXFrame mainFrame;
    StringTokenizer strTokens;
    MAXResultVector resultVector;
    Vector stationVector;
    int reportCount = 0;
    int queryCount = 0;
    int clientCount = 2;
    int descReqCount;
    int stationCount = 3;
    int sDescCount =0;

    //Methods
    //*******
    /** Initialise MAX Actor
     */
    public void init(MessageListener msgListen, MAXFrame mainFrame)
    {
	this.mainFrame = mainFrame;

	//Initialise Mote Interface
	mote = new MoteIF(PrintStreamMessenger.err, groupID);
	mote.registerListener(new MaxDescAnswer(), msgListen);
	mote.registerListener(new MaxAnswer(), msgListen);
	mote.registerListener(new MaxDescEnd(), msgListen);
	mote.registerListener(new MaxReport(), msgListen);
	mote.registerListener(new MaxSDescEnd(), msgListen);
    }

    /** Display Result
     */
    private void displayResult()
    {
	System.out.println("Display Result");

	//Remove No Count Results
	removeNoCount();

	mainFrame.listDisplay.setListData(resultVector);
	
    }

    /** Remove No Count Results
     */
    private void removeNoCount()
    {
	MAXResult result;

	for (int i =0; i < resultVector.size(); i++)
	{
	    result = (MAXResult) resultVector.get(i);
	    if (result.resultCount == 0)
	    {
		resultVector.remove(i);
		i--;
	    }
	}
    }

    /** Send Query
     */
    public void sendQuery(String text)
    {
	resultVector = new MAXResultVector();
	stationVector = new Vector();
	resultVector.linkStationVector(stationVector);
	strTokens = new StringTokenizer(text);
	reportCount = 0;
	sDescCount = 0;
	descReqCount = 0;

	sendNextQuery();
    }

    /** Send Next Query Word
     */
    private void sendNextQuery()
    {
	queryCount = 0;
	//Send individual tokens
	if (strTokens.hasMoreTokens())
	    sendQueryMessage(strTokens.nextToken());
	else
	    sendTagDescQuery();
    }

    /** Send Tag Descriptor Query
     */
    public void sendTagDescQuery()
    {
	MAXResult result;

	for (int i = 0; i < resultVector.size(); i++)
	{
	    result = (MAXResult) resultVector.get(i);
	    if (result.resultCount != 0)
	    {
		descReqCount++;
		sendDescQuery(result.moteID);
	    }
	}
    }


    /** Send Description Query for Stations
     */
    public void sendStationDescQuery()
    {
	MAXStation station;

	for (int i = 0; i < stationVector.size(); i++)
	{
	    station = (MAXStation) stationVector.get(i);
	    sendDescQuery(station.statID);
	}
    }

    /** Send Query Message
     */
    public void sendQueryMessage(String text)
    {
	MaxQuery queryMsg = new MaxQuery();
	queryMsg.setString_data(text.toLowerCase());

	System.out.println("Send Query Message");

	try
	{
	    mote.send(MoteIF.TOS_BCAST_ADDR, queryMsg);
	}
	catch (IOException ioException)
	{
	    System.err.println(ioException);
	}
    }

    /** Send Description Query
     */
    public void sendDescQuery(int moteID)
    {
	MaxDescQuery descQuery = new MaxDescQuery();

	System.out.print("Send Descriptor Query for Mote ");
	System.out.println(moteID);

	try
	{
	    mote.send(moteID, descQuery);
	}
	catch (IOException ioException)
	{
	    System.err.println(ioException);
	}
    }

    /** Act on MAXSDescEnd Message
     */
    public void receiveSDescEnd(int destAddr, MaxSDescEnd maxSDescEnd)
    {
	sDescCount++;

	System.out.print("Got maxSDescEnd number ");
	System.out.println(sDescCount);

	if (sDescCount == stationCount)
	    displayResult();
    }

    /** Act on MAXReport Message
     */
    public void receiveReport(int destAddr, MaxReport maxReport)
    {
	resultVector.addReport(maxReport.get_reportedMoteID(),maxReport.get_sourceMoteID(),maxReport.get_RSSI());

	System.out.print("Got maxReport from Mote ");
	System.out.print(maxReport.get_sourceMoteID());
	System.out.print(" on Mote ");
	System.out.print(maxReport.get_reportedMoteID());
	System.out.print(" with strength ");
	System.out.println(maxReport.get_RSSI());

	reportCount++;
	System.out.print(reportCount);
	System.out.print(" ");
	System.out.print(stationCount);
	System.out.print(" ");
	System.out.println(descReqCount);
	if (reportCount == stationCount*descReqCount)
	    sendStationDescQuery();
    }

    /** Act on MAXDescEnd Message
     */
    public void receiveDescEnd(int destAddr, MaxDescEnd maxDescEnd)
    {
	System.out.print("Got maxDescEnd message from Mote ");
	System.out.println( maxDescEnd.get_sourceMoteID());
    }

    /** Act on MAXDescAnswer Message
     */
    public void receiveDescAnswer(int destAddr, MaxDescAnswer maxDescAns)
    {
	if (stationVector.size() == 0)
	    resultVector.addDesc(maxDescAns.get_sourceMoteID(),maxDescAns.getString_data());
	else
	    resultVector.addStationDesc(maxDescAns.get_sourceMoteID(),maxDescAns.getString_data());

	System.out.print("Got maxDescAnswer message from Mote ");
	System.out.print( maxDescAns.get_sourceMoteID());
	System.out.print(" with String:");
	System.out.print(maxDescAns.getString_data());
	System.out.println(".");
    }

    /** Act on MAXAnswer Message
     */
    public void receiveAnswer(int destAddr, MaxAnswer maxAns)
    {
	int sourceMoteID;

	resultVector.add(maxAns.get_sourceMoteID(),maxAns.get_result());

	System.out.print("Got maxAnswer message from Mote ");
	System.out.print(maxAns.get_sourceMoteID());
	System.out.print(" with result ");
	System.out.println(maxAns.get_result());

	queryCount++;
	if (queryCount == clientCount)
	    sendNextQuery();
    }
}
