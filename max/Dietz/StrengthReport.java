package max.Dietz;

import java.util.*;

/**
   Class to report strength report
 */
public class StrengthReport
{
    //Members
    //*******
    /** Report strength
     */
    int strength;
    /** Reporting Mote ID
     */
    int reportMote;
    /** Link to Station Vector
     */
    Vector stationVector;

    //Methods
    //*******
    public StrengthReport (int reportMote, int strength, Vector stationVector)
    {
	this.reportMote = reportMote;
	this.strength = strength;
	this.stationVector = stationVector;
    }
}
