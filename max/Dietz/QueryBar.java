package max.Dietz;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
   Query Bar JPanel with a textbook and button
 */
public class QueryBar
    extends JPanel
{
    //Members
    //*******
    /** Descriptor of button */
    public final String buttonString = "Search";
    /** Maximum length of query allowed */
    public final int maxQueryLength = 20;
    /** JTextField for input of query */
    public JTextField text = new JTextField("",maxQueryLength);
    private JButton button = new JButton(buttonString);

    //Methods
    //*******
    /** Initialise Query Bar
     */
    public void init(ActionListener actionListen)
    {
	//Set Query Bar Panel
	this.setLayout(new BorderLayout());

	//Add Contents
	this.add(text,BorderLayout.CENTER);
	this.add(button,BorderLayout.EAST);

	//Link Listener
	button.addActionListener(actionListen);
    }
}
