package max.Dietz;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
   Main frame for MAX Dietz
 */
public class MAXFrame
    extends JFrame
{
    //Members
    //*******
    /** Initial height of frame */
    public final int frameHeight = 300;
    /** Initial width of frame */
    public final int frameWidth = 300;
    /** QueryBar on the frame */
    public QueryBar queryBar = new QueryBar();
    /** JList for displaying the result */
    public JList listDisplay = new JList();

    //Methods
    //*******
    /** Constructor with title of window
     */
    public MAXFrame(String title)
    {
	super(title);
    }

    /** Initialise Frame with components
     */
    public void init(ActionListener actionListen)
    {
	//Set Main Frame
	this.setSize(frameHeight, frameWidth);
	this.addWindowListener(new WinCloseEventHandler());

	//Add Contents
	this.getContentPane().setLayout(new BorderLayout());
	queryBar.init(actionListen);
	this.getContentPane().add(queryBar,BorderLayout.NORTH);
	this.getContentPane().add(listDisplay,BorderLayout.CENTER);

	//Show Main Frame
	this.show();
    }
}
