package max.Dietz;

import java.util.*;

public class MAXStation
{
    //Members
    //*******
    /** Descriptor of Station
     */
    public String statDesc="";
    /** Station ID
     */
    public int statID;

    //Methods
    //*******
    /** Constructor
     */
    public MAXStation(int stationID)
    {
	statID = stationID;
    }

    /** Add Descriptor Word
     */
    public void addDesc(String word)
    {
	String tmpString = new String();

	tmpString = tmpString.concat(word);
	tmpString = tmpString.concat(" ");
	statDesc = tmpString.concat(statDesc);
    }
}
