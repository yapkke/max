package max.Dietz;

import java.util.*;

/**
   Result Vector for MaxAnswer replies
 */
public class MAXResult
    implements Comparator
{
    //Members
    //*******
    /** Mote ID of Result
     */
    public int moteID;
    /** Descriptor of Result
     */
    public String moteDesc="";
    /** Count of Result
     */
    public int resultCount=0;
    /** Vector of strength report
     */
    public Vector strengthReports = new Vector();

    //Methods
    //*******
    /** Add Strength Report
     */
    public void addReport(int reportMote, int strength, Vector stationVector)
    {
	//Add New Strength Report
	strengthReports.add(new StrengthReport(reportMote, strength, stationVector));
    }

    /** Add Descriptor Word
     */
    public void addDesc(String word)
    {
	String tmpString = new String();

	tmpString = tmpString.concat(word);
	tmpString = tmpString.concat(" ");
	moteDesc = tmpString.concat(moteDesc);
    }

    /** Compare 2 of this objects
     */
    public int compare(Object obj1, Object obj2)
    {
	MAXResult maxResult1 = (MAXResult) obj1;
	MAXResult maxResult2 = (MAXResult) obj2;
	
	return maxResult1.resultCount-maxResult2.resultCount;
    }

    /** Compare with Equality
     */
    public boolean equals(Object object)
    {
	MAXResult maxResult = (MAXResult) object;

	return (this.resultCount == maxResult.resultCount);
    }

    /** Get Best Strength
     */
    public StrengthReport getBestStrength()
    {
	StrengthReport tmpReport, finalReport;

	finalReport = (StrengthReport) strengthReports.get(0);

	for (int i = 1; i < strengthReports.size(); i++)
	{
		tmpReport = (StrengthReport) strengthReports.get(i);
		if (tmpReport.strength < finalReport.strength)
		    finalReport = tmpReport;
	}

	return finalReport;
    }

    /** Get Best Strength String
     */
    public String getBestStrengthString()
    {
	String finalString = new String();
	MAXStation station;
	StrengthReport report = getBestStrength();
	
	for (int i = 0; i < report.stationVector.size(); i++)
	{
	    station = (MAXStation) report.stationVector.get(i);
	    if (station.statID == report.reportMote)
		finalString = station.statDesc;
	}

	return finalString;
    }

    /** Return string representation
     */
    public String toString()
    {
	String tmpString = new String(Integer.toString(resultCount));

	tmpString = tmpString.concat(" ");
	tmpString = tmpString.concat(moteDesc);

	tmpString = tmpString.concat(" @ ");
	tmpString = tmpString.concat(getBestStrengthString());

	return tmpString;
    }
}
