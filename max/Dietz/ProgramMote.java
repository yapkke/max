package max.Dietz;

import java.io.*;
import java.util.*;
import net.tinyos.util.*;
import net.tinyos.message.*;

/**
   The console program to program mote of specified ID with descriptor.
 */
public class ProgramMote
{
    //Members
    //*******
    /** Group ID
     */
    static public final int groupID = 0x13;
    private static ProgramListen listen = new ProgramListen();

    //Methods
    //*******
    /** Main function for programming mote with descriptor.
     */
    public static void main(String[] args)
    {
	MaxReset resetMsg = new MaxReset();

	//Check number of inputs
	if (args.length > 2)
	{
	    System.out.println("ProgramMote <MoteID> <Descriptor>");
	    System.exit(1);
	}

	//Initialise MoteIF
	listen.mote = new MoteIF(PrintStreamMessenger.err, groupID);
	listen.mote.registerListener(new MaxProgramAck(),listen);

	//Choose Type
	if (args.length < 2)
	{
	    try
	    {
		listen.mote.send(MoteIF.TOS_BCAST_ADDR, resetMsg);
	    }
	    catch (IOException ioException)
	    {
		System.err.println(ioException);
	    }

	    System.out.println("Reset System");
	    System.exit(0);
	}
	else
	{
	    //Grab Mote ID
	    listen.moteID = Integer.parseInt(args[0]);

	    //Create Strings for Transmission
	    listen.strings = new StringTokenizer(args[1]);

	    //Transmit Tokens
	    listen.sendNext();
	}
    }
}
