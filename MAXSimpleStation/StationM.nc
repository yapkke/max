includes maxMsg;
includes EEPROM;

module StationM
{
	provides interface StdControl;
	uses
	{
		interface Timer;
		interface Leds;

		interface StdControl as StringStoreControl;
		interface StringCtrl;

		interface StdControl as CommControl;
		interface ReceiveMsg as ReceiveProgram;
		interface SendMsg as SendProgramAck;
		interface ReceiveMsg as ReceiveDesc;
		interface SendMsg as SendDesc;
		interface ReceiveMsg as ReceiveDescEnd;
		interface SendMsg as SendSDescEnd;
		interface SendMsg as SendReport;
		interface ReceiveMsg as ReceiveReset;
	}
}
implementation
{
	//Declarations
	//************
	TOS_Msg descMsg;
	char descWord[WORD_SIZE];
	bool descLock;
	int descCount;

	TOS_Msg reportMsg;
	bool reportLock;
	uint8_t nextReportIn;
	uint8_t nextReportOut;
	uint16_t reportMote[REPORT_SIZE];
	uint16_t moteStrength[REPORT_SIZE];

	TOS_Msg ackMsg;
	char *programWord;
	bool programLock;

	//Tasks
	//*****
	//Reset System
	task void reset()
	{
		call StringCtrl.reset();
	}

	//Write Word to Buffer
	task void writeWord()
	{
		call StringCtrl.write(programWord);
	}

	//Send Station Descriptor End
	task void sendSDescEnd()
	{
		struct maxSDescEnd *descEnd;

		//Allocate payload
		atomic
		{
			descEnd = (struct maxSDescEnd *)descMsg.data;
		}

		//Attempt to send message
		if (call SendSDescEnd.send(TOS_BCAST_ADDR, sizeof(struct maxSDescEnd), &descMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Acknowledgement to Program
	task void sendProgramAck()
	{
		struct maxProgramAck *ack;

		//Allocate payload to message
		atomic
		{
			ack = (struct maxProgramAck *)ackMsg.data;
		}

		//Attempt to send message
		if (call SendProgramAck.send(TOS_BCAST_ADDR, sizeof(struct maxProgramAck), &ackMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Next Descriptor Word
	task void sendNextDesc()
	{
		if (descCount == 0)
		{
			descLock = FALSE;
			post sendSDescEnd();
		}
		else
		{
			call StringCtrl.read(descCount-1, descWord);
		}
	}

	//Send Descriptor
	task void sendDesc()
	{
		struct maxDescAnswer *descAns;

		//Allocate payload
		atomic
		{
			descAns = (struct maxDescAnswer *)descMsg.data;
		}

		//Set payload value
		descAns->sourceMoteID = TOS_LOCAL_ADDRESS;
		strcpy(descAns->data, descWord);

		//Attempt to send message
		if (call SendDesc.send(TOS_BCAST_ADDR, sizeof(struct maxDescAnswer), &descMsg))
		{
			call Leds.greenToggle();
		}
	}

	//Send Report
	task void sendReport()
	{
		struct maxReport *descReport;

		if (nextReportIn != nextReportOut)
		{
			if (!reportLock)
			{
				reportLock = TRUE;

				//Allocate payload
				atomic
				{
					descReport = (struct maxReport *)reportMsg.data;
				}

				//Set payload value
				descReport->sourceMoteID = TOS_LOCAL_ADDRESS;
				descReport->reportedMoteID = reportMote[nextReportOut];
				descReport->RSSI = moteStrength[nextReportOut];

				//Increment Out Pointer
				nextReportOut++;
				nextReportOut = (nextReportOut == REPORT_SIZE)?0:nextReportOut;

				//Attempt to send message
				if (call SendReport.send(TOS_BCAST_ADDR, sizeof(struct maxReport), &reportMsg))
				{
					call Leds.greenToggle();
				}
			}
		}
	}

	//StringStore Events
	//******************
	//Write Done
	event result_t StringCtrl.writeDone(result_t success)
	{
		programLock = FALSE;
		post sendProgramAck();

		return SUCCESS;
	}

	//Read Done
	event result_t StringCtrl.readDone(char *buffer, result_t success)
	{
		if (success == SUCCESS)
		{
			//Descriptor Read
			if (descLock)
			{
				post sendDesc();
			}
		}

		return SUCCESS;
	}

	//GenericComm Events
	//******************
	//Receive Descriptor End
	event TOS_MsgPtr ReceiveDescEnd.receive(TOS_MsgPtr msgPtr)
	{
		struct maxDescEnd *descEnd= (struct maxDescEnd *)msgPtr->data;
		int8_t reportOut = nextReportOut - 1;

		reportOut = (reportOut == -1)?REPORT_SIZE-1:reportOut;
		if (nextReportIn != reportOut)
		{
			atomic
			{
				//Grab Data
				reportMote[nextReportIn] = descEnd->sourceMoteID;
				moteStrength[nextReportIn] = msgPtr->strength;

				//Increment In Pointer
				nextReportIn++;
				nextReportIn = (nextReportIn == REPORT_SIZE)? 0:nextReportIn;
			}
			post sendReport();
		}

		return msgPtr;
	}

	//Receive Descriptor Query
	event TOS_MsgPtr ReceiveDesc.receive(TOS_MsgPtr msgPtr)
	{
		if (!descLock)
		{
			atomic
			{
				descLock = TRUE;
				descCount = call StringCtrl.size();
			}
			post sendNextDesc();
		}

		return msgPtr;
	}

	//Receive Reset
	event TOS_MsgPtr ReceiveReset.receive(TOS_MsgPtr msgPtr)
	{
		post reset();

		return msgPtr;
	}

	//Receive MaxProgram then Send Reply
	event TOS_MsgPtr ReceiveProgram.receive(TOS_MsgPtr msgPtr)
	{
		struct maxProgram *program= (struct maxProgram *)msgPtr->data; 

		if (!programLock)
		{
			atomic
			{
				programLock = TRUE;
				programWord = program->data;
			}
			post writeWord();
		}

		return msgPtr;
	}

	//Acknowledge station descriptor end sent
	event result_t SendSDescEnd.sendDone(TOS_MsgPtr sent, result_t success)
	{
		return SUCCESS;
	}

	//Acknowledge report sent
	event result_t SendReport.sendDone(TOS_MsgPtr sent, result_t success)
	{
		reportLock = FALSE;
		post sendReport();

		return SUCCESS;
	}

	//Acknowledge send descriptor done
	event result_t SendDesc.sendDone(TOS_MsgPtr sent, result_t success)
	{
		descCount--;
		post sendNextDesc();
		
		return SUCCESS;
	}

	//Acknowledge send program acknowledgement done
	event result_t SendProgramAck.sendDone(TOS_MsgPtr sent, result_t success)
	{
		return SUCCESS;
	}

	//Timer Events
	//************
	//Timer fired
	event result_t Timer.fired()
	{
		//Toggle LED
		call Leds.yellowToggle();

		return SUCCESS;
	}

	//StdControl Interface
	//********************
	//Initialise Component
	command result_t StdControl.init()
	{
		//Initialise Leds
		call Leds.init();
		call Leds.yellowOff();
		call Leds.greenOff();
		call Leds.redOff();

		//Initialise GenericComm
		call CommControl.init();

		//Initialise EEPROM
		programLock = FALSE;
		descLock = FALSE;
		reportLock = FALSE;
		nextReportIn = 0;
		nextReportOut = 0;
		call StringStoreControl.init();

		return SUCCESS;
	}

	//Start Component
	command result_t StdControl.start()
	{
		//Start Timer
		call Timer.start(TIMER_REPEAT, 1000);

		//Start GenericComm
		call CommControl.start();

		//Start EEPROM
		call StringStoreControl.start();

		return SUCCESS;
	}

	//Stop Component
	command result_t StdControl.stop()
	{
		//Stop Timer
		call Timer.stop();

		//Stop GenericComm
		call CommControl.stop();

		//Stop EEPROM
		call StringStoreControl.stop();

		return SUCCESS;
	}
}
