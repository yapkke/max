includes maxMsg;

configuration Station
{
	//No interface provided
}
implementation
{
	components Main, StationM, TimerC, LedsC, GenericComm, StringStore;

	Main.StdControl -> StationM;
	Main.StdControl -> TimerC;

	StationM.Leds -> LedsC;
	StationM.Timer -> TimerC.Timer[unique("Timer")];

	//String Store
	StationM.StringStoreControl -> StringStore;
	StationM.StringCtrl -> StringStore;

	//Communication Control
	StationM.CommControl -> GenericComm;
	StationM.ReceiveProgram -> GenericComm.ReceiveMsg[AM_MAXPROGRAM];
	StationM.SendProgramAck -> GenericComm.SendMsg[AM_MAXPROGRAMACK];
	StationM.ReceiveDesc -> GenericComm.ReceiveMsg[AM_MAXDESCQUERY];
	StationM.SendDesc -> GenericComm.SendMsg[AM_MAXDESCANSWER];
	StationM.ReceiveDescEnd -> GenericComm.ReceiveMsg[AM_MAXDESCEND];
	StationM.SendSDescEnd -> GenericComm.SendMsg[AM_MAXSDESCEND];
	StationM.SendReport -> GenericComm.SendMsg[AM_MAXREPORT];
	StationM.ReceiveReset -> GenericComm.ReceiveMsg[AM_MAXRESET];
}
